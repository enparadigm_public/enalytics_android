package com.enparadigm.enalytics;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.enparadigm.enalytics.db.DBHelper;
import com.enparadigm.enalytics.interfaces.AutoEvents;
import com.enparadigm.enalytics.utils.EnConfig;
import com.enparadigm.enalytics.utils.EnPrefs;
import com.enparadigm.enalytics.utils.EnalyticsActivityLifecycleCallbacks;
import com.enparadigm.enalytics.utils.EnalyticsMessage;
import com.enparadigm.enalytics.utils.UserPropertyImp;
import com.enparadigm.enalytics.utils.Worker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * Created by krishna on 06/07/18.
 */

public class Enalytics {
    private static final String TAG = "Enalytics";
    private static final Object LOCK = new Object();
    private Worker worker;
    private UserPropertyImp peopleImp;
    private EnConfig enConfig;
    private EnPrefs enPrefs;
    private static Enalytics sInstance;
    private Context ctx;

    public static Enalytics getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = new Enalytics(context);
                }
            }
        }
        return sInstance;
    }

    private Enalytics() {
    }

    private Enalytics(Context context) {
        this.ctx = context.getApplicationContext();
    }

    public Enalytics initialize(int appId, String userId, String distinctId, String eventsUrl, String propertiesUrl) {
        enPrefs = new EnPrefs(ctx);
        enConfig = new EnConfig();
        enConfig.setAppId(appId);
        enConfig.setEventsUrl(eventsUrl);
        enConfig.setPropertiesUrl(propertiesUrl);
        enConfig.setMinimumSessionDuration(10 * 1000); //default 10 sec
        enConfig.setSessionTimeoutDuration(Integer.MAX_VALUE); //default no timeout
        worker = new Worker(DBHelper.getInstance(ctx), enConfig);

        String uniqueId = TextUtils.isEmpty(userId) ? UUID.randomUUID().toString() : userId;
        enConfig.setUniqueueId(uniqueId);
        enPrefs.setUniqueId(uniqueId);
        enConfig.setDistinctId(distinctId);

        registerMixpanelActivityLifecycleCallbacks();
        if (DBHelper.isFirstOpen(ctx)) {
            //first time app opens
            track(AutoEvents.FIRST_OPEN);
        }
        Map<String, String> mDeviceInfo = Collections.unmodifiableMap(getDeviceInfo(ctx));
        peopleImp = new UserPropertyImp(enConfig, worker);
        //send device info
        peopleImp.setMap(mDeviceInfo);

        //flush pending events
        Message message = Message.obtain();
        message.what = EnalyticsMessage.TYPE_FLUSH;
        worker.sendMessage(message);

        return sInstance;
    }

    public void setEventsBatchLimit(int limit) {
        enConfig.setEventsBatchLimit(limit);
    }

    private void registerMixpanelActivityLifecycleCallbacks() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            if (ctx instanceof Application) {
                final Application app = (Application) ctx;
                EnalyticsActivityLifecycleCallbacks mEnalyticsLifecycleCallback = new EnalyticsActivityLifecycleCallbacks(this, enConfig);
                app.registerActivityLifecycleCallbacks(mEnalyticsLifecycleCallback);
            }
        }
    }

    @NonNull
    private Map<String, String> getDeviceInfo(Context ctx) {
        final Map<String, String> deviceInfo = new HashMap<>();
        deviceInfo.put("android_lib_version", String.valueOf(BuildConfig.VERSION_CODE));
        deviceInfo.put("android_os", "Android");
        deviceInfo.put("android_os_version", Build.VERSION.RELEASE == null ? "UNKNOWN" : Build.VERSION.RELEASE);
        deviceInfo.put("android_manufacturer", Build.MANUFACTURER == null ? "UNKNOWN" : Build.MANUFACTURER);
        deviceInfo.put("android_brand", Build.BRAND == null ? "UNKNOWN" : Build.BRAND);
        deviceInfo.put("android_model", Build.MODEL == null ? "UNKNOWN" : Build.MODEL);
        try {
            final PackageManager manager = ctx.getPackageManager();
            final PackageInfo info = manager.getPackageInfo(ctx.getPackageName(), 0);
            deviceInfo.put("android_app_version", info.versionName);
            deviceInfo.put("android_app_version_code", Integer.toString(info.versionCode));
            int lastVersionCode = enPrefs.getAppVersionCode();
            if (lastVersionCode == -1) {
                enPrefs.setAppVersionCode(info.versionCode);
            } else if (info.versionCode > lastVersionCode) {
                try {
                    track(AutoEvents.APP_UPDATED, new JSONObject().put(AutoEvents.VERSION_UPDATED, info.versionCode));
                    enPrefs.setAppVersionCode(info.versionCode);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (final PackageManager.NameNotFoundException e) {
            Log.d(TAG, "Exception getting app version name");
        }
        return deviceInfo;
    }

    public UserPropertyImp getPeople() {
        return peopleImp;
    }

    public void track(String eventName) {
        track(eventName, null);
    }

    public void trackMap(String eventName, Map<String, Object> properties) {
        if (null == properties) {
            track(eventName, null);
        } else {
            try {
                track(eventName, new JSONObject(properties));
            } catch (NullPointerException e) {
                Log.d(TAG, "Can't have null keys in the properties of trackMap!");
            }
        }
    }

    public void track(String eventName, JSONObject properties) {
        if (enConfig.isOptedOutOfTracking()) return;
        try {
            JSONObject messageObject = new JSONObject();
            messageObject.put("event", eventName);
            messageObject.put("user_id", enConfig.getUniqueueId());
            messageObject.put("distinctid", enConfig.getDistinctId());
            messageObject.put("timestamp", System.currentTimeMillis());
            if (properties != null) {
                Iterator<String> it = properties.keys();
                while (it.hasNext()) {
                    String key = it.next();
                    messageObject.put(key, properties.get(key));
                }
            }
            //enqueue the properties
            Message message = Message.obtain();
            message.what = EnalyticsMessage.TYPE_EVENTS;
            message.obj = messageObject;
            worker.sendMessage(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*public void identify(String userId) {
        if (TextUtils.isEmpty(userId)) return;
        String uniqueId = enPrefs.getUniqueId();
        if (TextUtils.isEmpty(uniqueId) || !uniqueId.equals(userId)) {
            enPrefs.setUniqueId(userId);
            enConfig.setUniqueueId(userId);
        }
    }*/

    public void onBackground() {
        //flush the events/properties
        Message message = Message.obtain();
        message.what = EnalyticsMessage.TYPE_FLUSH;
        worker.sendMessage(message);
    }

    public void onForeground() {

    }

    public void optOutOfTracking(boolean optOut) {
        enConfig.setOptedOutOfTracking(optOut);
    }
}
