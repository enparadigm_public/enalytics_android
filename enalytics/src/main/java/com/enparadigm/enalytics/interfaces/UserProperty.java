package com.enparadigm.enalytics.interfaces;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by krishna on 06/07/18.
 */

public interface UserProperty {

    void set(String propertyName, Object value);

    void setMap(Map<String, ? extends Object> properties);

    void set(JSONObject properties);

    void setOnce(String propertyName, Object value);

    void setOnceMap(Map<String, ? extends Object> properties);

    void setOnce(JSONObject properties);

    void increment(String name, double increment);

    void increment(Map<String, ? extends Number> properties);

    void unset(String name);
}
