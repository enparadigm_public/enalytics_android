package com.enparadigm.enalytics.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface API {
    //dummy base url, will get overridden by @Url param in method
    String BASE_URL = "https://www.google.com";

    @FormUrlEncoded
    @POST()
    Call<ResponseBody> pushDataToServer(@Url String url, @Field("app_id") int appId, @Field("data") String data);
}
