package com.enparadigm.enalytics.utils;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class SQLUtil {

    public static void executeSQL(SQLiteDatabase db , String query){
        SQLiteStatement stmt = db.compileStatement(query);
        stmt.execute();
    }

}
