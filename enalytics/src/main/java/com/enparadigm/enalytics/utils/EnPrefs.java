package com.enparadigm.enalytics.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by krishna on 06/07/18.
 */
public class EnPrefs {
    private static final String PREF_NAME = "enprefs";
    private static final String PREF_KEY_UNIQUE_ID = "unique_id";
    private static final String PREF_KEY_APP_VERSION_CODE = "app_version_code";
    private SharedPreferences sharedPreferences;

    public EnPrefs(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setUniqueId(String uniqueId) {
        Log.d("TAG", "setUniqueId: " + uniqueId);
        sharedPreferences.edit().putString(PREF_KEY_UNIQUE_ID, uniqueId).commit();
    }

    public String getUniqueId() {
        return sharedPreferences.getString(PREF_KEY_UNIQUE_ID, "");
    }

    public int getAppVersionCode() {
        return sharedPreferences.getInt(PREF_KEY_APP_VERSION_CODE, -1);
    }

    public void setAppVersionCode(int appVersionCode) {
        sharedPreferences.edit().putInt(PREF_KEY_APP_VERSION_CODE, appVersionCode).commit();
    }
}
