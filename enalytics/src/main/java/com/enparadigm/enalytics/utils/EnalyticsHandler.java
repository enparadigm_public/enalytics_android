package com.enparadigm.enalytics.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.enparadigm.enalytics.api.API;
import com.enparadigm.enalytics.api.ApiClient;
import com.enparadigm.enalytics.db.DBHelper;
import com.enparadigm.enalytics.db.model.MetaEvents;
import com.enparadigm.enalytics.db.model.MetaProperties;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by krishna on 06/07/18.
 */

public class EnalyticsHandler extends Handler {
    private static final String TAG = "EnalyticsHandler";
    private DBHelper dbHelper;
    private EnConfig enConfig;

    public EnalyticsHandler(EnConfig enConfig, DBHelper dbHelper, Looper looper) {
        super(looper);
        this.enConfig = enConfig;
        this.dbHelper = dbHelper;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case EnalyticsMessage.TYPE_EVENTS:
                JSONObject eventsJsonObject = (JSONObject) msg.obj;
                if (eventsJsonObject != null) {
                    long timestamp = System.currentTimeMillis();
                    try {
                        timestamp = eventsJsonObject.getLong("timestamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MetaEvents.insert(dbHelper, eventsJsonObject.toString(), timestamp);
                    Log.d(TAG, "enqueue event: " + timestamp + ", " + eventsJsonObject.toString());
                }
                break;
            case EnalyticsMessage.TYPE_PEOPLE_PROPERTY:
                JSONObject peopleJsonObject = (JSONObject) msg.obj;
                if (peopleJsonObject != null) {
                    long timestamp = System.currentTimeMillis();
                    try {
                        timestamp = peopleJsonObject.getLong("timestamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MetaProperties.insert(dbHelper, peopleJsonObject.toString(), timestamp);
                    Log.d(TAG, "enqueue property: " + timestamp + ", " + peopleJsonObject.toString());
                }
                break;
            case EnalyticsMessage.TYPE_FLUSH:
                final List<MetaEvents> metaEventsList = MetaEvents.getAllEventsToSync(dbHelper, enConfig.getEventsBatchLimit());
                API apiService = ApiClient.getApiClient().create(API.class);
                if (!metaEventsList.isEmpty()) {
                    MetaEvents.setSync(dbHelper, metaEventsList, MetaEvents.SYNC_STARTED);
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    String eventsJsonString = gson.toJson(metaEventsList, new TypeToken<List<MetaEvents>>() {
                    }.getType());
                    //send events to server
                    Call<ResponseBody> call = apiService.pushDataToServer(enConfig.getEventsUrl(), enConfig.getAppId(), eventsJsonString);

                    call.enqueue(new retrofit2.Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                MetaEvents.setSync(dbHelper, metaEventsList, MetaEvents.SYNC_DONE);
                                Log.d(TAG, "success: events pushed to server");
                            } else {
                                MetaEvents.setSync(dbHelper, metaEventsList, MetaEvents.SYNC_PENDING);
                                Log.d(TAG, "failure: events pushed to server");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d(TAG, "failure: events pushed to server " + t.getMessage());
                        }
                    });
                    Log.d(TAG, "flush events: " + eventsJsonString);
                }
                final List<MetaProperties> metaPropertiesList = MetaProperties.getAllPeopleToSync(dbHelper);
                if (!metaPropertiesList.isEmpty()) {
                    MetaProperties.setSync(dbHelper, metaPropertiesList, MetaProperties.SYNC_STARTED);
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    String userPropertiesString = gson.toJson(metaPropertiesList, new TypeToken<List<MetaProperties>>() {
                    }.getType());
                    //send user properties to server
                    Call<ResponseBody> call = apiService.pushDataToServer(enConfig.getPropertiesUrl(), enConfig.getAppId(), userPropertiesString);
                    call.enqueue(new retrofit2.Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                MetaProperties.setSync(dbHelper, metaPropertiesList, MetaProperties.SYNC_DONE);
                                Log.d(TAG, "success: properties pushed to server");
                            } else {
                                MetaProperties.setSync(dbHelper, metaPropertiesList, MetaProperties.SYNC_PENDING);
                                Log.d(TAG, "failure: properties pushed to server");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d(TAG, "failure: properties pushed to server " + t.getMessage());
                        }
                    });
                    Log.d(TAG, "flush properties: " + userPropertiesString);
                }

                //clear synced events and properties
                MetaEvents.clearSyncedEvents(dbHelper);
                MetaProperties.clearSyncedEvents(dbHelper);
                break;
        }
    }
}
