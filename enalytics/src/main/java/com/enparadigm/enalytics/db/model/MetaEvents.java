package com.enparadigm.enalytics.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.enparadigm.enalytics.db.DBHelper;
import com.enparadigm.enalytics.utils.SQLUtil;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MetaEvents {
    private static final String TAG = "Events";
    public static final String TABLE_NAME = "events";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + Column.COL__ID + " INTEGER PRIMARY KEY NOT NULL,"
            + Column.COL_DATA + " TEXT,"
            + Column.COL_CREATED_AT + " INTEGER NOT NULL,"
            + Column.COL_UPDATED_AT + " INTEGER NOT NULL,"
            + Column.COL_SYNC + " INTEGER DEFAULT 0"
            + ");";

    public static final int SYNC_PENDING = 0;
    public static final int SYNC_STARTED = 1;
    public static final int SYNC_DONE = 2;

    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("data")
    private String data;

    @SerializedName("created_at")
    private long createdAt;

    public static List<MetaEvents> getAllEventsToSync(DBHelper dbHelper, int limit) {
        List<MetaEvents> metaEventsList = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long forceUpdateTimestamp = System.currentTimeMillis() - 1800000; // 30 min = 1800000 millis
        Cursor cursor = db.query(TABLE_NAME, Column.columns, Column.COL_SYNC + "=" + SYNC_PENDING + " OR (" + Column.COL_SYNC + "=" + SYNC_STARTED + " AND " + Column.COL_UPDATED_AT + "<" + forceUpdateTimestamp + ")", null, null, null, null, String.valueOf(limit));
        if (cursor != null && cursor.moveToFirst()) {
            do {
                MetaEvents metaEvents = new MetaEvents();
                metaEvents.setId(cursor.getInt(cursor.getColumnIndex(Column.COL__ID)));
                metaEvents.setData(cursor.getString(cursor.getColumnIndex(Column.COL_DATA)));
                metaEvents.setCreatedAt(cursor.getLong(cursor.getColumnIndex(Column.COL_CREATED_AT)));
                metaEventsList.add(metaEvents);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return metaEventsList;
    }

    public static void clearSyncedEvents(DBHelper dbHelper) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        SQLUtil.executeSQL(db,"DELETE FROM " + TABLE_NAME + " WHERE " + Column.COL_SYNC + "=" + SYNC_DONE);
    }

    public static void insert(DBHelper dbHelper, String data, long timestamp) {
        ContentValues cv = new ContentValues();
        cv.put(Column.COL_DATA, data);
        cv.put(Column.COL_SYNC, 0);
        cv.put(Column.COL_CREATED_AT, timestamp);
        cv.put(Column.COL_UPDATED_AT, timestamp);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.insert(TABLE_NAME, null, cv);
    }

    public static void setSync(DBHelper dbHelper, List<MetaEvents> metaEventsList, int status) {
        if (metaEventsList != null && metaEventsList.size() > 0) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            long updatedAt = System.currentTimeMillis();
            db.beginTransaction();
            try {
                for (MetaEvents event : metaEventsList) {
                    ContentValues cv = new ContentValues();
                    cv.put(Column.COL_UPDATED_AT, updatedAt);
                    cv.put(Column.COL_SYNC, status);
                    int result = db.update(TABLE_NAME, cv, Column.COL__ID + "=? AND " + Column.COL_CREATED_AT + "=?", new String[]{String.valueOf(event.getId()), String.valueOf(event.getCreatedAt())});
                    Log.d(TAG, "setSync: " + result);
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
                Log.d(TAG, "transaction Error: " + e.getMessage());
            } finally {
                db.endTransaction();
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public static class Column {
        public static final String COL__ID = "_id";
        public static final String COL_DATA = "data";
        public static final String COL_SYNC = "sync";
        public static final String COL_CREATED_AT = "created_at";
        public static final String COL_UPDATED_AT = "updated_at";
        public static final String columns[] = {COL__ID, COL_DATA, COL_SYNC, COL_CREATED_AT, COL_UPDATED_AT};
    }
}
